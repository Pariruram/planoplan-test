import * as types from '../constants/ActionTypes'
import axios from 'axios'

export const getProducts = (cb) => dispatch => {
  axios.get('http://localhost:3000/products.json').then(response => {
    dispatch({
      type: types.GET_PRODUCTS,
      payload: response.data
    })
    cb();
  })
}

export const addToCart = (id, count) => dispatch => {
  dispatch({
    type: types.ADD_TO_CART,
    payload: { id: id, count: count }
  })
}

