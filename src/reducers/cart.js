import { ADD_TO_CART } from '../constants/ActionTypes'

const initialState = {
  cart: [],
  counts: {},
  total: 0
}

export default function cart(state = initialState, action) {
  switch (action.type) {
    case ADD_TO_CART:
      if (state.cart.indexOf(action.payload.id) !== -1) {
        let { id, count } = action.payload;
        let newCount = state.counts[id].count * 1 + count * 1;
        let obj = { [id]: { id, count: newCount } }
        let total = state.total * 1 + count * 1;
        return { ...state, counts: { ...state.counts, ...obj }, total: total }
      } else {
        let { id, count } = action.payload;
        let obj = { [id]: { id, count } }
        let total = state.total * 1 + count * 1;
        return { ...state, cart: [...state.cart, action.payload.id], counts: { ...state.counts, ...obj }, total: total }
      }
    default:
      return state
  }
}