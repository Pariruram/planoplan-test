import { GET_PRODUCTS } from '../constants/ActionTypes'

const initialState = {
  products: {}
}

export default function products(state = initialState, action) {
  switch (action.type) {
    case GET_PRODUCTS:
      return {
        ...state,
        products: action.payload
      }
    default:
      return state
  }
}