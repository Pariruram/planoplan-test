import React from 'react';

import './cartbtn.less'

function CartBtn({count}) {
  return (
    <div className='cart-btn'>
      <span className='hide-mob'>Корзина </span>
      <span className='cart-btn__icon'>
        <span className='cart-btn__num'>{count}</span>
      </span>
    </div>
  )
}

export default CartBtn;