import React from 'react';

import './productssearch.less'

function ProductsSearch({handleSearch}) {
  let textInput = null;
  function search() {
    handleSearch(textInput.value)
  }
  return (
    <div className='products-search'>
      <div className='input-group'>
        <input type='text' className='input-group__input' onChange={search} ref={(input)=> textInput = input} />
        <button type='submit' className='input-group__submit icon icon-search'><span className='hide-mob' onClick={search}>Найти</span></button>
      </div>
    </div>
  )
}

export default ProductsSearch;