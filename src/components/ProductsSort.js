import React from 'react';

import './productssort.less'

function ProductsSort({ sortBy, direction, handleSortClick }) {

  return (
    <div className='sort'>
      Сортировать:
      <SortType sort='по цене' name='price' direction={direction} active={(sortBy === 'price') ? 'active' : false} handleClick={handleSortClick} />
      <SortType sort='по дате' name='date' direction={direction} active={(sortBy === 'date') ? 'active' : false} handleClick={handleSortClick} />
    </div>
  )
}

function SortType({ sort, direction, active, name, handleClick }) {
  let dir = (direction > 0) ? 'up' : 'down';
  let dirClassName = (active) ? 'sort__type_' + dir : '';
  let act = (active) ? 'active' : '';
  function click() {
    handleClick(name)
  }
  return (
    <span className={`sort__type ${dirClassName} ${act}`} onClick={click}>{sort}</span>
  )
}

export default ProductsSort;