import React from 'react';

import Product from './Product'

import './productslist.less'

function ProductsList({products, handleAdd}) {
  let productsList;
  if (products.length > 0) {
    productsList = products.map(product => 
      <Product key={product.id} product={product} handleAdd={handleAdd}/>
    )
  } else {
    productsList = 'Пусто'
  }
  return (
      <div className='products-list'>
        {productsList}
      </div>
  )
}

export default ProductsList;