import React from 'react';

import './product.less'
import plusImg from '../images/plus.svg';
import minusImg from '../images/minus.svg';
import infoImg from '../images/info.svg';

function Product({ product, handleAdd }) {

  let counter = null;

  function increment() {
    counter.value++
  }

  function decrement() {
    (counter.value > 1) ? counter.value-- : counter.value;
  }

  function addToCart(e) {
    e.preventDefault();
    handleAdd(product.id, counter.value);    
  }

  return (
    <div className='product fadeInUp'>
      <div className={`product__image product__image-${product.type}`}>
        <img src={product.image} alt={product.title} />
      </div>
      <div className='product__cont'>
        <div className='product__title'>
          {product.title}
        </div>
        <div className='product__price'>
          {product.price}Р
          </div>
        <div className='product__to-cart-wrap'>
          <div className='product__count'>
            <Counter inputRef={(input) => { counter = input }} increment={increment} decrement={decrement}/>
          </div>
          <a href='#' className='product__to-cart btn icon icon-cart' onClick={addToCart}>
            В корзину
            </a>
        </div>
        <div className='product__info'>
          <Info text={product.infoText} />
        </div>
      </div>
    </div>

  )
}

function Info({ text }) {
  return (
    <div className='info'>
      <span href='@' className='info__icon'>
        <img src={infoImg} className='info__img' />
      </span>
      <div className='info__text'>{text}</div>
    </div>
  )
}

function Counter(props) {
  return (
    <div className='counter'>
      <button className='counter__btn counter__minus' onClick={props.decrement} >
        <img src={minusImg} className='counter__img' alt='-' />
      </button>
      <input type='text' className='counter__value' defaultValue='1'  ref={props.inputRef} />
      <button className='counter__btn counter__plus'onClick={props.increment} >
        <img src={plusImg} className='counter__img' alt='+' />
      </button>
    </div>
  )
}

export default Product;