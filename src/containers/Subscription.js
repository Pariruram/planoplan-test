import React from 'react';
import axios from 'axios';

import './subscription.less'


class Subscription extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      agreement: false,
      email: '',
      valid: false,
      submited: false
    }
    this.handleInput = this.handleInput.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  handleInput(event) {
    const target = event.target;
    const value = target.type === 'checkbox' ? target.checked : target.value;
    const name = target.name;

    if (validateEmail(this.state.email) && this.state.agreement) {
      this.setState({ valid: true });
    } else {
      this.setState({ valid: false });      
    }

    this.setState({
      [name]: value
    });
  }

  handleSubmit(e) {
    e.preventDefault();
    if (this.state.valid) {
      this.subscribe();
    }
  }

  subscribe() {
    axios.post('http://localhost:3000/subscribe', { email: this.state.email }).then(response => {
      if (response.data === 'OK') {
        this.setState({ submited: true })
      }
    })
  }

  render() {

    if (this.state.submited) {
      return (
        <section className='subscription'>
          <div className='subscription__thx fadeInUp'>
            Спасибо за подписку!
          </div>
        </section>
      )
    }

    return (
      <section className='subscription'>
        <form action='' onSubmit={this.handleSubmit}>
          <h2>
            Подписка
        </h2>
          <p>Оставьте ваш e-mail, чтобы получать наши новости</p>
          <p>
            <input name='agreement' className='input-checkbox' type='checkbox' id='agreement' checked={this.state.agreement} onChange={this.handleInput} />
            <label htmlFor='agreement'>Согласен на обработку данных</label>
          </p>
          <div className='input-group input-group_small input-group_border-white'>
            <input name='email' type='text' className='input-group__input' placeholder='Ваш e-mail' onChange={this.handleInput} />
            <input type='submit' className='input-group__submit icon icon-mail' value='Подписаться' disabled={!this.state.valid} />
          </div>
        </form>
      </section>
    )
  }
}


function validateEmail(email) {
  var re = /^(([^<>()\[\]\\.,;:\s@"]+(\.[^<>()\[\]\\.,;:\s@"]+)*)|(".+"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
  return re.test(email);
}

export default Subscription;