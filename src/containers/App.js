import React from 'react';
import { connect } from 'react-redux'
import { bindActionCreators } from 'redux'

import ProductsSearch from '../components/ProductsSearch'
import CartBtn from '../components/CartBtn'
import ProductsSort from '../components/ProductsSort'
import ProductsList from '../components/ProductsList'
import Subscription from '../containers/Subscription'

import * as appActions from '../actions/index'

import './app.less'

class App extends React.Component {

  constructor(props) {
    super(props)
    this.props = props;
    this.state = {
      items: props.products,
      sortBy: 'price',
      sortDir: 1
    }
    this.visibleProducts = props.products;
    this.getProducts = this.props.getProducts.bind(this)
    this.addToCart = this.props.addToCart.bind(this)
    this.handleSearch = this.handleSearch.bind(this)
    this.handleSortClick = this.handleSortClick.bind(this)
  }

  componentDidMount() {
    this.getProducts(() => this.setState({ items: this.props.products }));
  }


  handleSearch(str) {
    let filteredProducts = this.props.products;
    filteredProducts = filteredProducts.filter(function (item) {
      return item.title.toLowerCase().search(
        str.toLowerCase()) !== -1;
    });

    this.setState({ items: filteredProducts });
  }

  handleSortClick(type) {
    const dir = this.state.sortDir * -1;
    if (this.state.sortBy === type) {
      this.setState({ sortDir: dir })
    } else {
      this.setState({ sortBy: type })
    }
    const sorted = this.state.items.sort(function (a, b) {
      if (a[type] === b[type]) { return 0; }
      return a[type] > b[type] ? dir : dir * -1;
    });

    this.setState({ items: sorted })
  }

  render() {
    return (
      <div className='app'>
        <header className='container'>
          <h1>Товары года</h1>
        </header>
        <section className='container'>
          <div className='top-bar'>
            <div className='top-bar__search'>
              <ProductsSearch handleSearch={this.handleSearch} />
            </div>
            <div className='top-bar__cart'>
              <CartBtn count={this.props.total} />
            </div>
          </div>
          <ProductsSort sortBy={this.state.sortBy} direction={this.state.sortDir} handleSortClick={this.handleSortClick} />
          <ProductsList products={this.state.items} handleAdd={this.addToCart} />
        </section>
        <footer className='footer'>
          <div className='container'>
            <div className='footer-wrap'>
              <Subscription />
              <section className='social'>
                <h2>Социальные сети</h2>
                <a href='#' className='social__icon'></a>
                <a href='#' className='social__icon'></a>
                <a href='#' className='social__icon'></a>
                <a href='#' className='social__icon'></a>
                <a href='#' className='social__icon'></a>
              </section>
            </div>
          </div>
        </footer>
      </div>
    );
  }
}

function mapStateToProps(state) {
  return { ...state.products, ...state.cart }
}

function mapDispatchToProps(dispatch) {
  return bindActionCreators(appActions, dispatch);
}

export default connect(mapStateToProps, mapDispatchToProps)(App)